# installing zsh shell.
sh install-zsh-v1.sh

# removing all installed themes.
rm ~/.oh-my-zsh/themes/*

# copying the custom theme file to the theme directory.
cp jubin_zsh_theme.zsh-theme ~/.oh-my-zsh/themes/

# removing the echo command of the zsh shell while it is selecting the random theme.
cp oh-my-zsh.sh ~/.oh-my-zsh 