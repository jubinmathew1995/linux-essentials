# Arch-based-distro

### To install zsh and add custom styling just execute the following command.

    > sh arch-terminal-styling.sh
    
### remove all packages in your cache
	
	> pacman -Scc

### remove unused packages which are installed.
	
	> sudo pacman -R $(pacman -Qdtq)

### to view unused packages in arch
	
	> pacman -Qdtq

	and 

	> sudo pacman -Qdtq

### to use command similar to 'apt update'
	
	> sudo pacman -Syy