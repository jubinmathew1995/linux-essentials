# Ubuntu-Essentials


### system  

#####******** keeping the source list up-to-date

```
sudo apt update

or

sudo apt-get update
```

#####******** keeping the software up-to-date
```
sudo apt upgrade
sudo apt dist-upgrade
```

#####******** having problem with wifi not getting detected. Either goto to aditional driver and switch the proprietary driver for the wifi or
```
sudo apt install dkms
sudo dpkg -i bcmwl-kernel-source_6.30.223.248+bdcom-0ubuntu0.2_amd64.deb
```

#####******** install media codes
```
sudo apt-get install ubuntu-restricted-extras
```

#####******** install important additional software
```  
sudo apt install htop git nethogs vim mpv ranger deluge curl evince gnome-terminal gdebi python3 python3-dev python3-pip
```


### software installs
#####******** install software offline using dpkg

```
sudo dpkg -i software_package.deb
```

#####******** install git

```
sudo apt install git
```
#####******** install Atom-editor
```
sudo add-apt-repository ppa:webupd8team/atom
sudo apt update; sudo apt install atom
```
#####******** install sublime-editor
```
sudo add-apt-repository -y ppa:webupd8team/sublime-text-2
sudo apt-get update
sudo apt-get install sublime-text
```
#####******** To install Node-JS and NPM
```
sudo apt-get install python-software-properties
curl -sL https://deb.nodesource.com/setup_7.x | sudo -E bash -
sudo apt-get install nodejs
```  
#####******** for testing Node-JS install
```
node -v
npm -v
node --debug http_server.js  
```
#####******** To install Node-JS and NPM using node version manager(nvm)
  ```
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.8/install.sh | bash

or

wget -qO- https://raw.githubusercontent.com/creationix/nvm/v0.33.8/install.sh | bash

nvm install node
nvm --version
npm --version
node --version
  ```
#####******** install yarn package manager for Node-JS
```
sudo npm install -g yarn
```
#####******** install chrome using CLI
```
  wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
  sudo sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" /etc/apt/sources.list.d/google-chrome.list'
  sudo apt-get update
  sudo apt-get install google-chrome-stable
```
#####******** install unity-tweek-tool
```
sudo apt-get install unity-tweak-tool
```
#####******** install VLC and VLC-plugin
```
sudo apt-get install vlc browser-plugin-vlc
```
### important commands

#####******** check which all ports are in use
```
netstat -l
```
#####******** check for which all ports are in use for localserver
```
netstat -l | grep local
```
#####******** To start, stop and restart the network-manager
```
sudo service network-manager start
sudo service network-manager stop
sudo service network-manager restart
```
#####******** to start,stop and restart mongodb server
```
sudo service mongod start
sudo service mongod stop
sudo service mongod restart
```
#####******** to change the position of the unity launcher from left or bottom.
```
gsettings set com.canonical.Unity.Launcher launcher-position Bottom
gsettings set com.canonical.Unity.Launcher launcher-position Left
```
#####******** to make the username visible in the task bar
```
gsettings set com.canonical.indicator.session show-real-name-on-panel true
gsettings set com.canonical.indicator.session show-real-name-on-panel false
```
#####******** enable 1 click minimize
```
gsettings set org.compiz.unityshell:/org/compiz/profiles/unity/plugins/unitysh
ell/ launcher-minimize-window true
```
### others
#####******** terminal styling - copy the following in .bashrc

> \# setting either the ip of the system
> l=\`hostname -I\`


> \# for coloured prompt
>> using markdown
>>> PS1='${debian_chroot:+($debian_chroot)}\\[\033[01;32m\\]\u@${l:-\`hostname\`}\\[\033[00m\\] : \\[\033[01;34m\\]\w\\[\033[00m\\]\n\$ '

>> using raw
>>> PS1='${debian_chroot:+($debian_chroot)}\\[\033[01;32m\\]\u@${l:-`hostname`}\\[\033[00m\\] : \\[\033[01;34m\\]\w\\[\033[00m\\]\n\$ '

>\# for plain prompt
>> using markdown
>>> PS1='${debian_chroot:+($debian_chroot)}\u@${l:-\`hostname\`} : \w\n\$ '

>> using raw
>>> PS1='${debian_chroot:+($debian_chroot)}\u@${l:-`hostname`} : \w\n\$ '

### Important websites.

website for ubuntu theme.
> https://askubuntu.com/questions/800730/dark-gtk-theme-for-ubuntu-16-04-unity
